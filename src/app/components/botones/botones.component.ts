import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit {
  
  constructor() { }
  valor: number = 0;
  mensaje: boolean = false

  ngOnInit(): void {
  }

  maximo(){
    if(this.valor === 50){
    this.mensaje = true;
    } else{
      this.valor = this.valor + 1;
      this.mensaje = false; 
    }
  }

  minimo(){
    if(this.valor === 0){
    this.mensaje = true;
    } else{
      this.valor = this.valor - 1;
      this.mensaje = false;
    }
  }
}

